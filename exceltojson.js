const workbook = XLSX.readFile("./data/TTBBVQ11.xlsx");

const worksheet = workbook.Sheets[workbook.SheetNames[0]];
const labels = [
  "title",
  "model",
  "manufacturer",
  "origin",
  "manufacturedYear",
  "startUseTime",
  "startUseState",
  "quantity",
  "originalPrice",
  "totalPrice",
  "faculty"
];

const data = XLSX.utils.sheet_to_json(worksheet, {
  range: 2,
  header: labels,
  defval: null
});