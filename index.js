const fs = require("fs");
const axios = require("axios");
const async = require("async");
// const XLSX = require("xlsx");

const rawData = fs.readFileSync("./data/list.json");

const devices = JSON.parse(rawData);

let newDevices = [];

devices.map((device) => {
  if (device.startUseState === "Mới") {
    device.startUseState = true;
  } else if (device.startUseState === "Cũ") {
    device.startUseState = false;
  }

  device.startUseTime = new Date(device.startUseTime);
  device.manufacturedYear = parseInt(device.manufacturedYear).toString();
  device.title = device.title.replace(/^\s+|\s+$/g, "");
  let newDevice = { ...device };
  delete newDevice.quantity;
  delete newDevice.totalPrice;
  newDevices.push(newDevice);

  if (device.quantity > 1) {
    for (let i = 2; i <= device.quantity; i++) {
      let newDevice = { ...device };
      delete newDevice.quantity;
      delete newDevice.totalPrice;
      newDevice.title = `${device.title} ${i}`;
      newDevices.push(newDevice);
    }
  }
});

const addDeviceBody = (device) =>
  `
  mutation {
    addDevice(
      deviceInput: ${JSON.stringify(device, null, 8).replace(
        /\"([^(\")"]+)\":/g,
        "$1:"
      )}
    ) {
      id
    }
  }
`;

let accessToken =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmM2ZjOTRkZjBlZGYwNzA2ZDVmNWM1NSIsImVtYWlsIjoibmd1eWVuY29uZ2xvbmc5NUBnbWFpbC5jb20iLCJyb2xlIjoiQURNSU4iLCJpYXQiOjE1OTgwMTgxMDYsImV4cCI6MTU5ODAxOTAwNn0.eCbJ8RG5O1EmPOTJ_FiredmJYvSI1Tk1wS2D6BlUKos";
var request = require("request");

async.forEachOf(
  newDevices,
  (device, i, callback) => {
    console.log(i);
    var options = {
      method: "POST",
      url: "http://127.0.0.1:8000/graphql",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        query: addDeviceBody(newDevices[0]),
        variables: {},
      }),
    };
    request(options, function (error, response) {
      if (error) throw new Error(error);
      console.log(response.body);
      callback();
    });
  },
  (err) => {
    console.log(err);
  }
);

// axios
//   .post(
//     "http://127.0.0.1:8000/graphql",
//     JSON.stringify({
//       query: addDeviceBody(device),
//       variables: {},
//     }),
//     {
//       headers: {
//         "Content-Type": "application/json",
//         Authorization: `Bearer ${accessToken}`,
//       },
//     }
//   )
//   .then((result) => {
//     console.log(result.data.data.addDevice);
//   });

// devices.forEach((device) => {
//   console.log(addDeviceBody(device));
// });

// console.log(JSON.stringify({ query: addDeviceBody(devices[0]) }));
