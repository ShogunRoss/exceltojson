import pandas as pd

lables = ['title', 'model', 'manufacturer', 'origin', 'manufacturedYear', 'startUseTime', 'startUseState', 'quantity', 'originalPrice', 'totalPrice', 'falcuty']
excel_data_df = pd.read_excel('./data/TTBBVQ11.xlsx', sheet_name='2018', skiprows=2, header=None, usecols='B:L', names=lables)

result = excel_data_df.to_json(orient='records', force_ascii=False)

file = open('./data/list.json', 'x')

file.write(result)

file.close()